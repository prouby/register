/* register.c -- Game launcher.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <dlfcn.h>

#include <SDL2/SDL.h>

#define WIDTH  640
#define HEIGHT 480
#define TITLE  "ASM Game tools"

#define SDL_ASSERT(exp)                                         \
  if (!(exp))                                                   \
    {                                                           \
      fprintf(stderr, "[error][SDL] %s", SDL_GetError());       \
      abort();                                                  \
    }

#define DLSYM_IMPORT_FUNCTION(ptr, type, name)          \
  ptr = type dlsym(dlh, name);                          \
  error = dlerror();                                    \
  if (error != NULL)                                    \
    {                                                   \
      fprintf (stderr, "[error][dl] %s\n", error);      \
      return -1;                                        \
    }

#define _TIMER_START(start)                     \
  start = clock();

#define _TIMER_STOP(timer, start)               \
  timer += (clock() - start);

/********************/
/* GLOBAL VARIABLES */
/********************/

/* DLib */
void *dlh;
const char * (*license)(void);
const char * (*name)(void);
int (*init)(short *, short *);
int (*render)(void);
SDL_Color (*color)(short);

/* SDL */
SDL_Window *window;
SDL_Renderer *renderer;
SDL_GameController *controller;

long long int frames = 0;

short *screen_tab;
short *input_tab; /* {up, down, left, right, a, b}; */

/* Timers */
unsigned long int timer_render = 0;
unsigned long int timer_all = 0;

/*******/
/* LIB */
/*******/

int
load_game(const char *file)
{
  char *error;

  dlh = dlopen(file, RTLD_LAZY);
  if (!dlh)
    {
      fprintf(stderr, "[error][dl] %s\n", dlerror());
      return -1;
    }

  dlerror();

  DLSYM_IMPORT_FUNCTION(license, (const char * (*)(void)),    "license");
  DLSYM_IMPORT_FUNCTION(name,    (const char * (*)(void)),    "name");
  DLSYM_IMPORT_FUNCTION(init,    (int (*)(short *, short *)), "init");
  DLSYM_IMPORT_FUNCTION(render,  (int (*)(void)),             "render");
  DLSYM_IMPORT_FUNCTION(color,   (SDL_Color (*)(short)),      "color");

  printf ("[info][game] Game loaded '%s' with license %s\n",
          (*name) (), (*license) ());

  return 0;
}

void
quit_game()
{
  dlclose(dlh);
  dlh = NULL;
}

void
reset_screen()
{
  int w = WIDTH / 2;
  int h = HEIGHT / 2;

  memset(screen_tab, 0, w*h*sizeof(short));
}

void
draw_screen_tab()
{
  int x, y;
  int sx, sy;
  int w = WIDTH / 2;
  int h = HEIGHT / 2;

  short cur;
  short prev = 0;

  SDL_Color c;
  SDL_Point points[4];

  for (x = 0; x < w; x++)
    {
      sx = x*2;

      points[0].x = sx;
      points[1].x = sx;
      points[2].x = sx+1;
      points[3].x = sx+1;

      for (y = 0; y < h; y++)
        {
          cur = screen_tab[x+y*w];

          if (cur == 0)
            continue;

          if (cur != prev)
            {
              c = (*color)(screen_tab[x+y*w]);
              SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, 255);
            }

          sy = y*2;

          points[0].y = sy;
          points[1].y = sy+1;
          points[2].y = sy;
          points[3].y = sy+1;

          SDL_RenderDrawPoints(renderer, points, 4);

          prev = cur;
      }
    }
}

void
reset_input()
{
  int i;
  for (i = 0; i < 6; i++)
    input_tab[i] = 0;
}

void
events_key_mod (SDL_Event e, short v)
{
  switch (e.key.keysym.sym)
    {
    case SDLK_UP:
      input_tab[0] = v;
      break;
    case SDLK_DOWN:
      input_tab[1] = v;
      break;
    case SDLK_LEFT:
      input_tab[2] = v;
      break;
    case SDLK_RIGHT:
      input_tab[3] = v;
      break;
    case SDLK_w:
      input_tab[4] = v;
      break;
    case SDLK_x:
      input_tab[5] = v;
      break;
    default:
      break;
    }
}

void
events_controller_mod(SDL_Event e, short v)
{
  switch (e.cbutton.button)
    {
    case SDL_CONTROLLER_BUTTON_DPAD_UP:
      input_tab[0] = v;
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
      input_tab[1] = v;
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
      input_tab[2] = v;
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
      input_tab[3] = v;
      break;
    case SDL_CONTROLLER_BUTTON_A:
      input_tab[4] = v;
      break;
    case SDL_CONTROLLER_BUTTON_B:
      input_tab[5] = v;
      break;
    default:
      break;
    }
}

int
events ()
{
  SDL_Event event;

  while (SDL_PollEvent (&event))
    {
      switch (event.type)
        {
        case SDL_QUIT:
          return -1;
        case SDL_KEYDOWN:
          events_key_mod (event, 1);
          break;
        case SDL_KEYUP:
          events_key_mod (event, 0);
          break;
        case SDL_CONTROLLERBUTTONDOWN:
          events_controller_mod(event, 1);
          break;
        case SDL_CONTROLLERBUTTONUP:
          events_controller_mod(event, 0);
          break;
        default:
          break;
        }
    }

  return 0;
}

SDL_GameController *
open_controller_rec (int i)
{
  SDL_GameController * control;

  if (i > SDL_NumJoysticks()) {
    return NULL;
  }

  if (SDL_IsGameController(i))
    {
      control = SDL_GameControllerOpen(i);
      if (control)
          return control;
    }

  return open_controller_rec (++i);
}


int
draw()
{
  int ret;
  SDL_Color c;
  unsigned long int render_start;
  unsigned long int all_start;

  _TIMER_START (all_start);

  if (events() == -1)
    return 0;

  reset_screen();

  c = (*color)(0);

  SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, 255);
  SDL_RenderClear(renderer);

  _TIMER_START (render_start);
  ret = (*render) ();
  _TIMER_STOP (timer_render, render_start);

  if (ret != 0)
    return ret;

  draw_screen_tab();
  _TIMER_STOP (timer_all, all_start);

  SDL_RenderPresent(renderer);

  frames += 1;
  return draw();
}

int
init_sdl()
{
  int ret;

  ret = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER);
  SDL_ASSERT(ret == 0);

  window = SDL_CreateWindow((*name)(),
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            WIDTH, HEIGHT,
                            SDL_WINDOW_SHOWN);
  SDL_ASSERT(window != NULL);

  renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED
                                | SDL_RENDERER_PRESENTVSYNC);
  SDL_ASSERT(renderer != NULL);

  controller = open_controller_rec(0);
  if (controller != NULL)
    printf ("[info][game] Controller found.\n");

  return ret;
}

void
quit_sdl()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);

  SDL_Quit();
}


/********/
/* MAIN */
/********/

void
usage(int argc, char **argv)
{
  fprintf (stderr, "Usage: %s <file.so>\n", argv[0]);
  exit(EXIT_FAILURE);
}

int
main (int argc, char **argv)
{
  int ret = 0;

  timer_all = clock();

  if (argc < 2 || argv[1] == NULL)
    usage(argc, argv);

  /* INIT */
  if (load_game(argv[1]) != 0)
    exit(EXIT_FAILURE);

  screen_tab = malloc (sizeof(short) * ((WIDTH/2) * (HEIGHT/2)));
  if (screen_tab == NULL)
    {
      fprintf (stderr, "[error] Malloc\n");
      exit(EXIT_FAILURE);
    }

  input_tab = malloc (sizeof(short) * 6);
  if (input_tab == NULL)
    {
      fprintf (stderr, "[error] Malloc\n");
      exit(EXIT_FAILURE);
    }

  init_sdl();

  /* Init game */
  if ((*init) (screen_tab, input_tab) != 0)
    {
      fprintf (stderr, "[error][game] Game init failed\n");
      exit(EXIT_FAILURE);
    }

  /* DRAW */
  ret = draw();
  if (ret < 0)
    {
      printf ("[error][game] Render return %d\n", ret);
    }

  timer_all = clock() - timer_all;

  printf ("[timer][all] %lf second\n", timer_all/(double)CLOCKS_PER_SEC);
  printf ("[timer][render] %lf second\n", timer_render/(double)CLOCKS_PER_SEC);
  printf ("[timer][render] '%s' render use %lf%% of total renderer time\n",
          (*name) (), (timer_render/(double)timer_all)*100);

  /* FREE */
  free (screen_tab);
  quit_sdl();
  quit_game();

  return ret;
}

/* register.c ends here */
