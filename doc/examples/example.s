# example.s --- Game demo for game launcher on X86_64.      -*- mode: asm -*-
#
# Copyright (C) 2019  Pierre-Antoine Rouby
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

        .file "example.s"

        ##########
        ## DATA ##
        ##########

        .section	.rodata
_str_license:   .string "GPLv3+"
_str_name:      .string "Game demo for x86_64"

                        ## a b g r
_colors:        .long   0x00222222 # Background color
                .long   0x000000ff # First color (1)
                .long   0x0000aa00 # 2
                .long   0x00ff0000 # 3
                .long   0x00000000 # 4
                .long   0x0033e8ff # 5
                .long   0x00ffffff # 6

        ############
        ## SPRITE ##
        ############

_sprite_x:      .short  3,0,0,0,3
                .short  0,3,0,3,0
                .short  0,0,3,0,0
                .short  0,3,0,3,0
                .short  3,0,0,0,3

_smiley:        .short  0,0,0,0,0,5,5,5,5,5,5,0,0,0,0,0
                .short  0,0,0,0,5,5,5,5,5,5,5,5,0,0,0,0
                .short  0,0,5,5,5,5,5,5,5,5,5,5,5,5,0,0
                .short  0,0,5,5,5,5,5,5,5,5,5,5,5,5,0,0
                .short  0,5,5,5,5,4,5,5,5,5,4,5,5,5,5,0
                .short  5,5,5,5,4,5,4,5,5,4,5,4,5,5,5,5
                .short  5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
                .short  5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
                .short  5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
                .short  5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
                .short  5,5,5,5,4,5,5,5,5,5,5,4,5,5,5,5
                .short  0,5,5,5,5,4,5,5,5,5,4,5,5,5,5,0
                .short  0,0,5,5,5,5,4,4,4,4,5,5,5,5,0,0
                .short  0,0,5,5,5,5,5,5,5,5,5,5,5,5,0,0
                .short  0,0,0,0,5,5,5,5,5,5,5,5,0,0,0,0
                .short  0,0,0,0,0,5,5,5,5,5,5,0,0,0,0,0

        .section        .data
_rand_next:     .quad 0x1
_rand_max:      .quad 32767

## Game global variable
_screen:        .quad 0x0
_input:         .quad 0x0
_status:        .quad 0x0
_frames:        .quad 0x0
_x:             .quad 0
_y:             .quad 0

        ###############
        ## FUNCTIONS ##
        ###############

        .section        .text
	.global	        license, name, init, render, color

        ###########
        ## TOOLS ##
        ###########

## unsigned int i_time ()
i_time:
        movq    $201, %rax      # sys_time
        xor     %rdi, %rdi      # NULL
        syscall
        ret


## Pseudo random implementation based on POSIX.1-2001 example.
##   TODO: Optimisation of mul and div.
## int rand ()
i_rand:
        leaq    _rand_next(%rip), %rcx
        movq    (%rcx), %rax    # Get next value
        movq    $1103515245, %rdx
        mul     %rdx            # Next * 1103515245
        movq    %rax, %rdx
        addq    $6667, %rdx     # %rdx + 6667
        movq    %rdx, (%rcx)    # Save new next value
        movq    %rdx, %rax      # Next as dividend
        xor     %rdx, %rdx
        movq    $65536, %rcx    # 65536 as divisor
        div     %rcx
        xor     %rdx, %rdx
        movq    $32768, %rcx    # 32768 as divisor
        div     %rcx            # (%rax is result of previous div)
        movq    %rdx, %rax      # Get modulo result
        ret

##  void srand (unsigned int seed)
i_srand:
        cmpq    $0, %rdi
        je      i_srand_time
        leaq    _rand_next(%rip), %rax
        movq    %rdi, (%rax)    # Save seed as random next
        jmp     i_srand_end
i_srand_time:                   # Get time as random seed
        call    i_time          # Returns time in %rax
        leaq    _rand_next(%rip), %rcx
        movq    %rax, (%rcx)
i_srand_end:
        xor     %rax, %rax
        ret


        #############
        ## DISPLAY ##
        #############

## int draw_pixel (int x, int y, short color)
draw_pixel:
        pushq   %rdx            # Save arg 3

        movq    $640, %rax
        imulq   %rsi            # iy = y * (320 * sizeof(short))
        movq    %rax, %rsi      # Save result on %rsi

        movq    $2, %rax
        imulq   %rdi            # ix = x * sizeof(short)
        movq    %rax, %rdi      # Save result on %rdi

        addq    %rsi, %rdi      # index = ix + iy

        leaq    _screen(%rip), %rcx
        movq    (%rcx), %rax    # Get screen pointer
        addq    %rdi, %rax      # screen[index]

        popq    %rdx
        cmpb    $0, %dl
        je      draw_pixel_end
        movb    %dl, (%rax)      # screen[index] = 1
draw_pixel_end:
        movq    $0, %rax
        ret


## int draw_rect_5_5 (int x, int y)
draw_rect_5_5:
        pushq   %r12

        cmpq    $320, %rdi      # x > 320px
        jg      draw_rect_5_5_end
        movq    %rdi, %r8       # Save x on r8

        cmpq    $240, %rsi      # y > 240px
        jg      draw_rect_5_5_end
        movq    %rsi, %r9       # Save y on r9

        ## Start draw
        xor     %r12, %r12      # Init lines (y) counter
        jmp     draw_rect_5_5_line
draw_rect_5_5_next_line:
        addq    $0x1, %r12      # Add line
draw_rect_5_5_line:
        xor     %r11, %r11      # Init x index counter
        cmpq    $0x5, %r12
        je      draw_rect_5_5_end
draw_rect_5_5_on_line:
        cmpq    $0x5, %r11
        je      draw_rect_5_5_next_line

        ## Draw pixel
        movq    %r8, %rdi       # Get start x
        addq    %r11, %rdi      # add x step
        movq    %r9, %rsi       # Get start y
        addq    %r12, %rsi      # add y step
        movb    $1, %dl         # Color 1
        call    draw_pixel      # Draw

        addq    $0x1, %r11
        jmp     draw_rect_5_5_on_line
draw_rect_5_5_end:
        popq    %r12
        xor     %rax, %rax
        ret

## int draw_tex_5_5 (short *tex, int x, int y)
draw_tex_5_5:
        pushq   %r12

        cmpq    $0, %rdi
        je      draw_tex_5_5_end
        movq    %rdi, %r10

        cmpq    $320, %rsi      # x > 320px
        jg      draw_tex_5_5_end
        movq    %rsi, %r8       # Save x on r8

        cmpq    $240, %rdx      # y > 240px
        jg      draw_tex_5_5_end
        movq    %rdx, %r9       # Save y on r9

        ## Start draw
        xor     %r12, %r12      # Init lines (y) counter
        jmp     draw_tex_5_5_line
draw_tex_5_5_next_line:
        addq    $0x1, %r12      # Add line
draw_tex_5_5_line:
        xor     %r11, %r11      # Init x index counter
        cmpq    $0x5, %r12
        je      draw_tex_5_5_end
draw_tex_5_5_on_line:
        cmpq    $0x5, %r11
        je      draw_tex_5_5_next_line

        ## tex[x+y*w]
        movq    $10, %rax       # w = w*sizeof(short)
        imulq   %r12            # y*w
        movq    %rax, %rdx      # keep result on %rdx
        movq    %r11, %rax      # x
        addq    %rax, %rax      # x * 2 (2 = sizeof(short))
        addq    %rdx, %rax      # x + y*w
        addq    %r10, %rax      # tex[x + y*w]
        movb    (%rax), %dl     # Copy color
        cmpb    $0, %dl
        jle     draw_tex_5_5_skip # Skip pixels draw if color == 0

        ## Draw pixel
        movq    %r8, %rdi       # Get start x
        addq    %r11, %rdi      # add x step
        movq    %r9, %rsi       # Get start y
        addq    %r12, %rsi      # add y step

        call    draw_pixel      # Draw

draw_tex_5_5_skip:
        addq    $0x1, %r11
        jmp     draw_tex_5_5_on_line
draw_tex_5_5_end:
        popq    %r12
        xor     %rax, %rax
        ret


## int draw_tex_16_16 (short *tex, int x, int y)
draw_tex_16_16:
        pushq   %r12

        cmpq    $0, %rdi
        je      draw_tex_16_16_end
        movq    %rdi, %r10

        cmpq    $320, %rsi      # x > 320px
        jg      draw_tex_16_16_end
        movq    %rsi, %r8       # Save x on r8

        cmpq    $240, %rdx      # y > 240px
        jg      draw_tex_16_16_end
        movq    %rdx, %r9       # Save y on r9

        ## Start draw
        xor     %r12, %r12      # Init lines (y) counter
        jmp     draw_tex_16_16_line
draw_tex_16_16_next_line:
        addq    $0x1, %r12      # Add line
draw_tex_16_16_line:
        xor     %r11, %r11      # Init x index counter
        cmpq    $16, %r12     # 16 pixels
        je      draw_tex_16_16_end
draw_tex_16_16_on_line:
        cmpq    $16, %r11     # 16 pixels
        je      draw_tex_16_16_next_line

        ## tex[x+y*w]
        movq    $32, %rax       # w = w*sizeof(short)
        imulq   %r12            # y*w
        movq    %rax, %rdx      # keep result on %rdx
        movq    %r11, %rax      # x
        addq    %rax, %rax      # x * 2 (2 = sizeof(short))
        addq    %rdx, %rax      # x + y*w
        addq    %r10, %rax      # tex[x + y*w]
        movb    (%rax), %dl
        cmpb    $0, %dl         # Skip drawing if color == 0
        jle     draw_tex_16_16_skip_draw

        ## Draw pixel
        movq    %r8, %rdi       # Get start x
        addq    %r11, %rdi      # add x step
        movq    %r9, %rsi       # Get start y
        addq    %r12, %rsi      # add y step

        call    draw_pixel      # Draw

draw_tex_16_16_skip_draw:
        addq    $0x1, %r11
        jmp     draw_tex_16_16_on_line
draw_tex_16_16_end:
        popq    %r12
        xor     %rax, %rax
        ret

        ####################
        # PUBLIC FUNCTIONS #
        ####################

## const char * license ()
license:
        leaq    _str_license(%rip), %rax
        ret


## const char * name ()
name:
        leaq    _str_name(%rip), %rax
        ret

## {short,short,short,short} color (int i)
color:
        movq    $4, %rax
        imulq   %rdi            # i*4
        leaq    _colors(%rip), %rcx
        addq    %rax, %rcx      # colors[index]
        movl    (%rcx), %eax
        ret

## int init (short *screen, short *input)
init:
        movq    %rdi, %rax
        cmpq    $0, %rax        # NULL pointer
        je      init_end_error
        ## Register screen ptr
        leaq    _screen(%rip), %rax
        movq    %rdi, (%rax)

        movq    %rsi, %rax
        cmpq    $0, %rax        # NULL pointer
        je      init_end_error
        ## Register input ptr
        leaq    _input(%rip), %rax
        movq    %rsi, (%rax)

        ## Random seed
        xor     %rdi, %rdi
        call    i_srand

init_end:
        leaq    _status(%rip), %rax
        movq    $0, (%rax)

        movq    $0, %rax
        ret
init_end_error:
        movq    $-1, %rax
        ret


## int render ()
render:
        ## Read keys
        leaq    _input(%rip), %rcx
        cmpq    $0, (%rcx)      # NULL ptr ?
        je      render_end_error
        movq    (%rcx), %rdx    # Get input table ptr

        ## Check keys, and move rect
render_key_up:
        movb    (%rdx), %al
        cmpb    $0, %al         # KEY_UP
        je      render_key_down # Goto next key test

        leaq    _y(%rip), %rax  # Get y value
        movq    (%rax), %rcx
        cmpq    $0, %rcx        # Out of tab
        jle     render_key_down
        subq    $2, %rcx        # Move on screen
        movq    %rcx, (%rax)

render_key_down:
        addq    $2, %rdx
        movb    (%rdx), %al
        cmpb    $0, %al         # KEY_DOWN
        je      render_key_left

        leaq    _y(%rip), %rax
        movq    (%rax), %rcx
        cmpq    $224, %rcx
        jge     render_key_left
        addq    $2, %rcx
        movq    %rcx, (%rax)

render_key_left:
        addq    $2, %rdx
        movb    (%rdx), %al
        cmpb    $0, %al         # KEY_LEFT
        je      render_key_right

        leaq    _x(%rip), %rax
        movq    (%rax), %rcx
        cmpq    $0, %rcx
        jle     render_key_right
        subq    $2, %rcx
        movq    %rcx, (%rax)

render_key_right:
        addq    $2, %rdx
        movb    (%rdx), %al
        cmpb    $0, %al         # KEY_RIGHT
        je      render_key_end

        leaq    _x(%rip), %rax
        movq    (%rax), %rcx
        cmpq    $304, %rcx
        jge     render_key_end
        addq    $2, %rcx
        movq    %rcx, (%rax)
render_key_end:

        ## ## Draw rect
        ## call    i_rand
        ## movq    %rax, %rdi
        ## call    i_rand
        ## movq    %rax, %rsi
        ## call    draw_rect_5_5

        leaq    _smiley(%rip), %rdi
        leaq    _x(%rip), %rcx  # x
        movq    (%rcx), %rsi
        leaq    _y(%rip), %rcx  # y
        movq    (%rcx), %rdx
        call    draw_tex_16_16

        leaq    _frames(%rip), %rax
        movq    (%rax), %rcx
        addq    $1, %rcx
        movq    %rcx, (%rax)

render_end:
        movq    $0, %rax
        ret
render_end_error:
        movq    $-1, %rax
        ret

# example.s ends here.
